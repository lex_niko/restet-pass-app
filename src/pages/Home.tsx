import React, { useState } from "react";
import "./Home.css";
import Typography from "@material-ui/core/Typography";
import {
  Grid,
  Paper,
  Button,
  Theme,
  makeStyles,
  createStyles,
  TextField,
} from "@material-ui/core";
import { useHistory } from "react-router";
import { RESET_PASS_URL } from "../utils/urls";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      justifyContent: "center",
    },
    paper: {
      marginTop: theme.spacing(5),
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    item: {
      padding: theme.spacing(1),
    },
    button: {
      marginTop: theme.spacing(1),
    },
    icon: {
      color: "#fff",
      borderRadius: "50%",
      background: "#ccc",
      padding: ".5rem",
    },
  })
);

export default function Home() {
  const classes = useStyles();
  const [code, setCode] = useState<string>("");
  const { push } = useHistory();

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    push(RESET_PASS_URL.format({}, { code }));
  };

  return (
    <div className={classes.root}>
      <Grid container justify="center">
        <Grid item xs={8} sm={6} md={4}>
          <Paper className={classes.paper}>
            <Grid container>
              <form
                className={classes.root}
                autoComplete="off"
                onSubmit={onSubmit}
              >
                <Grid item xs={12} className={classes.item}>
                  <Typography variant="h6" component="h1">
                    Please enter reset code:
                  </Typography>
                </Grid>
                <Grid item xs={12} className={classes.item}>
                  <TextField
                    fullWidth
                    id="code"
                    label="Code"
                    value={code}
                    onChange={(e) => setCode(e.currentTarget.value)}
                    required
                  />
                </Grid>
                <Grid item xs={12} className={classes.item}>
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    type="submit"
                  >
                    Reset Password
                  </Button>
                </Grid>
              </form>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
