import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.css";
import Home from "./pages/Home";
import reportWebVitals from "./reportWebVitals";
import ResetPass from "./pages/ResetPass";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { HOME_URL, RESET_PASS_URL } from "./utils/urls";

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route path={HOME_URL.urlTemplate} exact>
          <Home />
        </Route>
        <Route path={RESET_PASS_URL.urlTemplate}>
          <ResetPass />
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
