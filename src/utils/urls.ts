import { createPath } from "rd-url-utils";
export const HOME_URL = createPath("/");
export const RESET_PASS_URL = createPath<{}, { code: string }>("/reset");
